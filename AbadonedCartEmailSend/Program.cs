﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Net;
using Newtonsoft.Json.Linq;
using System.IO;
using Dapper;
using BrontoLibrary.Models;
using BrontoLibrary;
using AbadonedCartEmailSend.Models;
using System.Data;
using TeamsHelper;


namespace AbadonedCartEmailSend
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SendAbandonedCartEmails();
            }
            catch (Exception ex)
            {
                string title = "Error in AbandonedCart Program";
                string text = $"{ex.Message}";
                string color = "red";
                TeamsMessage teamsMessage = new TeamsMessage(title, text, color, teamsUrl);
                teamsMessage.LogToMicrosoftTeams(teamsMessage);
            }
        }

        private static void SendAbandonedCartEmails()
        {
            List<User> usersWithAbandonedCart = GetUsersWithAbandonedCart();

            usersWithAbandonedCart = RemoveUsersWithRecentQualifiedCallsLogged(usersWithAbandonedCart);

            usersWithAbandonedCart = RemoveUsersWhoHaveBeenSentEmailRecently(usersWithAbandonedCart);

            List<User> usersWithCartDetailsAdded = RetrieveCartDetailsFromCartService(usersWithAbandonedCart);

            const string transactionConnection = @"Data Source = srv-pro-sqls-01; Initial Catalog = Micro_transactions; Integrated Security=True";
            usersWithCartDetailsAdded = RemoveUsersWithRecentOrMatchingEstimates(usersWithCartDetailsAdded, transactionConnection);

            usersWithCartDetailsAdded = RemoveUsersWhoPurchasedItemsInCartRecently(usersWithCartDetailsAdded, transactionConnection);

            List<User> deDuppedUsers = usersWithCartDetailsAdded.GroupBy(x => x.EmailAddress).Select(y => y.First()).ToList();

            foreach (var user in deDuppedUsers)
            {
                List<LineItem> lineItems = new List<LineItem>();
                foreach (var item in user.Cart)
                {
                    LineItem cartItem = new LineItem
                    {
                        SiteProductId = item.VariantId,
                        Quantity = item.Quantity,
                        ImageUrl = $"https://assets.supply.com/{item.ImageUrl}",
                        Price = item.DiscountedPrice,
                        SKU = item.ManufacturerSku,
                        Description = item.DisplayName
                    };
                    lineItems.Add(cartItem);
                }
                Order customerWithCart = new Order
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.EmailAddress,
                    Department = user.UserType == "5" ? "29" : "27",
                    LineItems = lineItems
                };
                var messageId = customerWithCart.Department == "29" ? "0bdb03eb00000000000000000000001101a8" : "e139d4b10024d3003cb78567cfa11d9c";
                try
                {
                    var result = BrontoConnector.SendAbandonedCartEmail(customerWithCart, messageId).Result;
                }
                catch (Exception ex)
                {
                    string title = "Error in single Abandoned Cart Email";
                    string text = $"{ex.Message}";
                    string color = "yellow";
                    TeamsMessage teamsMessage = new TeamsMessage(title, text, color, teamsUrl);
                    teamsMessage.LogToMicrosoftTeams(teamsMessage);
                }
            }
        }

        private static List<User> GetUsersWithAbandonedCart()
        {
            const string websiteProfilesConnection = @"Data Source = srv-pro-sqls-01; Initial Catalog = Micro_profiles; Integrated Security=True";
            List<User> usersWithCart = new List<User>();
            using (var conn = new SqlConnection(websiteProfilesConnection))
            {
                conn.Open();
                usersWithCart = conn.Query<User>("GetContactsForAbandonedCart", commandTimeout: 180, commandType: CommandType.StoredProcedure).ToList();
                conn.Close();
            }

            if (usersWithCart.Count < 1)
            {
                throw new Exception("GetContactsForAbandonedCart Failed");
            }

            return usersWithCart;
        }

        private static List<User> RemoveUsersWithRecentQualifiedCallsLogged(List<User> users)
        {
            //Remove folks with a recent Call log as long as it is not one of these values
            int[] CallLogTypesToIgnore = { (int)CallLogType.CalledOnce, (int)CallLogType.CalledThreeOrMoreTimes,
                                                    (int)CallLogType.CalledTwice, (int)CallLogType.Disconnected,
                                                        (int)CallLogType.NeedsCalling, (int)CallLogType.NoAnswer,
                                                            (int)CallLogType.NoNumber, (int)CallLogType.Voicemail,
                                                                (int)CallLogType.NoCall, (int)CallLogType.WrongNumber };
            for (int i = users.Count - 1; i >= 0; i--)
            {
                bool invalidCallLogType = CallLogTypesToIgnore.Contains(users[i].CallLogId);
                if (invalidCallLogType) continue;

                bool callLoggedBeforeCartModified = users[i].CallLogTimeStamp < users[i].LastModified;
                if (callLoggedBeforeCartModified) continue;

                Console.WriteLine($"removed {users[i].EmailAddress}. Call Log {users[i].CallLogId}, {users[i].CallLogTimeStamp}. Cart Date {users[i].LastModified}");
                users.RemoveAt(i);
            }
            return users;
        }

        private static List<User> RemoveUsersWhoHaveBeenSentEmailRecently(List<User> users)
        {
            const string emailActivityConnection = @"Data Source=srv-pro-sqls-02;Initial Catalog=BRONTO;Integrated Security=True";
            //Remove folks who received email in last 30 days
            for (int i = users.Count - 1; i >= 0; i--)
            {
                DateTime lastSend = new DateTime();
                using (var conn = new SqlConnection(emailActivityConnection))
                {
                    conn.Open();
                    try
                    {
                        lastSend = conn.QuerySingle<DateTime>("SELECT MAX(CreatedDate) FROM [BRONTO].[dbo].[RawRecipientData] WHERE ActivityType = @ActivityType AND MessageIdBronto in ('e139d4b10024d3003cb78567cfa11d9c', '0bdb03eb00000000000000000000001101a8') AND Email = @email", new { ActivityType = "send", Email = users[i].EmailAddress });
                    }
                    catch (Exception e)
                    {
                        lastSend = DateTime.Today.AddMonths(-3);
                    }
                    conn.Close();
                }
                if (lastSend > DateTime.Today.AddMonths(-1))
                {
                    Console.WriteLine($"removed {users[i].EmailAddress}. sent: {lastSend}. Cart Date {users[i].LastModified}");
                    users.RemoveAt(i);
                }
            }
            return users;
        }

        private static List<User> RetrieveCartDetailsFromCartService(List<User> users)
        {
            const string cartServiceUrl = @"https://service-cart.supply.com/getcartbyemailaddress?email=";
            List<User> usersWithCartDetails = new List<User>();
            foreach (var user in users)
            {
                WebResponse response = null;
                try
                {
                    WebRequest request = WebRequest.Create($"{cartServiceUrl}{user.EmailAddress}");
                    response = request.GetResponse();
                    List<CartItem> cart = new List<CartItem>();
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string cartString = reader.ReadToEnd();
                        JObject jsonCart = JObject.Parse(cartString);
                        string itemArrayString = jsonCart["lineitems"].ToString();
                        JArray jsonItemArray = JArray.Parse(itemArrayString);
                        cart = jsonItemArray.ToObject<List<CartItem>>();
                    }
                    if (cart.Count() > 0)
                    {
                        user.Cart = cart;
                        usersWithCartDetails.Add(user);
                    }
                }
                catch (WebException e)
                {
                    response = (HttpWebResponse)e.Response;
                }
                finally
                {
                    response.Close();
                }
            }

            if (usersWithCartDetails.Count < 1)
            {
                throw new Exception("No contacts with recent carts, call to cart service failed");
            }
            return usersWithCartDetails;
        }

        private static List<User> RemoveUsersWithRecentOrMatchingEstimates(List<User> users, string connectionString)
        {
            //Remove users with open estimates having at least one item matches with cart
            //ONly look at estimate and SO with created date AFTER 
            for (int i = users.Count - 1; i >= 0; i--)
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    var estimates = conn.Query<Models.Estimate>("uspGetEstimatesByEmail", new { email = users[i].EmailAddress }, commandType: CommandType.StoredProcedure).ToList();
                    foreach (var estimate in estimates)
                    {
                        bool recentEstimate = estimate.LastModified > users[i].LastModified;
                        if (recentEstimate)
                        {
                            Console.WriteLine($"removed {users[i].EmailAddress}. recent estimate: {estimate.LastModified}. Cart Date {users[i].LastModified}");
                            users.RemoveAt(i);
                            break;
                        }
                        bool openEstimate = estimate.Status == "Open";
                        if (openEstimate)
                        {
                            var items = conn.Query<EstimateItems>("uspGetEstimateDetailByEstimateId", new { estimateId = estimate.Id }, commandType: CommandType.StoredProcedure).ToList();
                            bool cartItemMatchesOpenEstimate = users[i].Cart.Any(x => items.Any(y => y.ItemId.ToString() == x.VariantId));
                            if (cartItemMatchesOpenEstimate)
                            {
                                Console.WriteLine($"removed {users[i].EmailAddress}. existing item: {estimate.LastModified}. Cart Date {users[i].LastModified}");
                                users.RemoveAt(i);
                                break;
                            }
                        }
                    }
                    conn.Close();
                }
            }
            return users;
        }

        private static List<User> RemoveUsersWhoPurchasedItemsInCartRecently(List<User> users, string connectionString)
        {
            for (int i = users.Count - 1; i >= 0; i--)
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    var salesOrders = conn.Query<SalesOrder>("uspGetOrdersByEmail", new { email = users[i].EmailAddress }, commandType: CommandType.StoredProcedure).ToList();
                    foreach (var salesOrder in salesOrders)
                    {
                        var dateCheck = DateTime.Today.AddMonths(-2);
                        if (salesOrder.CreatedDate > users[i].LastModified)
                        {
                            Console.WriteLine($"removed {users[i].EmailAddress}. recent purchase: {salesOrder.CreatedDate}. Cart Date {users[i].LastModified}");
                            users.RemoveAt(i);
                            break;
                        }
                        if (salesOrder.CreatedDate > dateCheck)
                        {
                            var items = conn.Query<SalesOrderItem>("uspGetOrderDetailByOrderNumber", new { orderNumber = salesOrder.SalesOrderNumber }, commandType: CommandType.StoredProcedure).ToList();
                            bool purchasedItemInCart = users[i].Cart.Any(x => items.Any(y => y.ItemId.ToString() == x.VariantId));
                            if (purchasedItemInCart)
                            {
                                Console.WriteLine($"removed {users[i].EmailAddress}. bought item in cart: {salesOrder.CreatedDate}. Cart Date {users[i].LastModified}");
                                users.RemoveAt(i);
                                break;
                            }
                        }
                    }
                    conn.Close();
                }
            }
            return users;
        }
        private enum CallLogType
        {
            NoCall = 0,
            NeedsCalling = 5,
            CalledOnce = 6,
            CalledTwice = 7,
            NoAnswer = 8,
            WrongNumber = 9,
            Disconnected = 10,
            CalledThreeOrMoreTimes = 11,
            NoNumber = 13,
            Voicemail = 15
        }
        private static string teamsUrl = "https://outlook.office.com/webhook/2e3cbfd5-55cb-4a1b-a2fd-c683dbffd345@3c2f8435-994c-4552-8fe8-2aec2d0822e4/IncomingWebhook/d035124ea28c49dc852fabd7a85c05c4/35aa24e2-d8c8-48b7-8dbe-c577c684ca90";
    }
}