﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbadonedCartEmailSend.Models
{
    public class CartItem
    {
        public string VariantId { get; set; }
        public int Quantity { get; set; }
        public string Url { get; set; }
        public string ImageUrl { get; set; }
        public string ManufacturerSku { get; set; }
        public decimal BasePrice { get; set; }
        public decimal DiscountedPrice { get; set; }
        public decimal AverageRating { get; set; } = 0;
        public string DisplayName { get; set; } = "";
        public string TagLine { get; set; } = "";
        public decimal YourPrice { get; set; } = 0;
        public bool IsInStock { get; set; } = false;
        public string VariantColor { get; set; } = "";
    }
}
