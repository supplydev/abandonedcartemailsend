﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbadonedCartEmailSend.Models
{
    public class EstimateItems
    {
        public int LineId { get; set; }

        public int EstimateId { get; set; }

        public int ItemId { get; set; }

        public int Quantity { get; set; }

        public int Available { get; set; }

        public string Description { get; set; }

        public string Notes { get; set; }

        public decimal Rate { get; set; }

        public double Amount { get; set; }

        public bool IsDiscontinued { get; set; }

        public decimal ProSalesPrice { get; set; }

        public decimal NBSSalesPrice { get; set; }

        public string EstimateNumber { get; set; }

        public string Model { get; set; }

        public int? NetSuiteLineIndex { get; set; }

        public decimal? CostEstimate { get; set; }

        public bool? DropShip { get; set; }

        public bool? FFA { get; set; }

    }

}
