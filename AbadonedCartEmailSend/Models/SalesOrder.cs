﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbadonedCartEmailSend.Models
{
    public class SalesOrder
    {
        public int Id { get; set; }

        public string SalesOrderNumber { get; set; }

        public string OrderNumber { get; set; }

        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public string Status { get; set; }

        public string TrackingNumber { get; set; }

        public decimal Total { get; set; }

        public decimal SubTotal { get; set; }

        public decimal Tax { get; set; }

        public decimal Shipping { get; set; }

        public decimal EstCost { get; set; }

        public decimal EstGrossProfit { get; set; }

        public string SalesRep { get; set; }

        public string CreatedBy { get; set; }

        public string Department { get; set; }

        public string BillTo { get; set; }

        public string BillAddress { get; set; }

        public string BillCity { get; set; }

        public string BillState { get; set; }

        public string BillZip { get; set; }

        public string ShipTo { get; set; }

        public string ShipAddress { get; set; }

        public string ShipCity { get; set; }

        public string ShipState { get; set; }

        public string ShipZip { get; set; }

        public string CustomerPhoneNumber { get; set; }

        public string LowesVendorNumber { get; set; }

        public string LowesShipToAddress { get; set; }

        public string LowesOrderByAddress { get; set; }

        public string LowesPOandJobNumber { get; set; }

        public string JobName { get; set; }

        public int? EstimateId { get; set; }

        public decimal? Handling { get; set; }

        public decimal? Discount { get; set; }

        public string ExpectedShipDate { get; set; }

        public string PrintedTicketDate { get; set; }

        public string Email { get; set; }

        public bool? DeletedInNetSuite { get; set; }

        public string IPAddress { get; set; }

        public string Microsite { get; set; }
    }
}
