﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbadonedCartEmailSend.Models
{
    public class SalesOrderItem
    {
        public int LineId { get; set; }

        public int SalesOrderId { get; set; }

        public string SalesOrderNumber { get; set; }

        public int ItemId { get; set; }

        public string ItemName { get; set; }

        public decimal Revenue { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal CostEstimate { get; set; }

        public int Committed { get; set; }

        public int Fulfilled { get; set; }

        public int Backordered { get; set; }

        public int Quantity { get; set; }

        public bool IsDiscontinued { get; set; }

        public bool Closed { get; set; }

        public string LowesLineNumBPN { get; set; }

        public string ItemNotes { get; set; }

        public bool? LineDeletedInNetSuite { get; set; }
    }
}
