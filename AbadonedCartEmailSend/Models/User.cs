﻿using System;
using System.Collections.Generic;
using System.Text;
using AbadonedCartEmailSend.Models;

namespace AbadonedCartEmailSend.Models
{
    public class User
    {
        public string UserType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public DateTime LastActivity { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastModified { get; set; }
        public DateTime CallLogTimeStamp { get; set; }
        public int CallLogId { get; set; }
        public List<CartItem> Cart { get; set; }
    }
}
