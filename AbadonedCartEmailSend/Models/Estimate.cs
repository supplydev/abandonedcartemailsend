﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbadonedCartEmailSend.Models
{
    public class Estimate
    {
        public int Id { get; set; }

        public string EstimateNumber { get; set; }

        public int? CustomerId { get; set; }

        public string CustomerName { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastModified { get; set; }

        public DateTime ExpirationDate { get; set; }

        public DateTime? ExpectedCloseDate { get; set; }

        public string Status { get; set; }

        public decimal? Subtotal { get; set; }

        public decimal? Discount { get; set; }

        public decimal? Tax { get; set; }

        public decimal? ShippingCost { get; set; }

        public decimal? HandlingCost { get; set; }

        public decimal? Total { get; set; }

        public string ShipTo { get; set; }

        public decimal? EstimatedExtendedCost { get; set; }

        public int? SalesOrderId { get; set; }

        public string JobName { get; set; }

        public string Email { get; set; }

        public string ShipAddress { get; set; }

        public string Message { get; set; }

        public string ShipAddr1 { get; set; }

        public string ShipAddr2 { get; set; }

        public string ShipAddr3 { get; set; }

        public string ShipCity { get; set; }

        public string ShipState { get; set; }

        public string ShipCountry { get; set; }

        public string ShipZip { get; set; }

        public string Department { get; set; }

        public string ProSalesFirstName { get; set; }

        public string ProSalesLastName { get; set; }

        public string ProSalesEmail { get; set; }

        public string Memo { get; set; }

        public string CreatedBy { get; set; }

        public decimal? EstGrossProfitPercent { get; set; }

        public decimal? EstGrossProfit { get; set; }

        public string DiscountRate { get; set; }

        public decimal? HMWShippingCost { get; set; }

        public bool? ClosedByBusinessUser { get; set; }

        public string ReasonForDiscount { get; set; }

    }

}
